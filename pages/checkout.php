<?php
	session_start();
    require_once("../includes/init.php")
?>

<?php if (Auth::getInstance()->isLoggedIn()): ?>
	<html lang="da-DA">
	    <?php include "../includes/header.php" ?>
	    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	    <?php include "../includes/navbar.php" ?>

		<div class="download-section">
			<section id="download" class="container content-section text-center">
				<div class="download-focuslock">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<?php if ($_SESSION["product"] == "paid"): ?>
									<?php Licens::generateOrderID(); Licens::generateLicens(); ?>
									<br><br><br>
									<h2>Kurv:</h2>
									<p>&bull; Forældrepakken, indenholder: tid- og kalenderfunktionen<br></p>

									<p>
										Ialt:<span class="invisible">lills</span>100 DKK
									</p>
									<ul class="list-inline banner-social-buttons">
										<li>
											<form method="post" action="https://payment.architrade.com/paymentweb/start.action" accept-charset="utf-8">
												<input type="hidden" name="merchant" value="90234185" />
												<input type="hidden" name="accepturl" value="https://www.focuslock.dk/pages/process?orderid=<?php print($_SESSION["order"]); ?>" />
												<input type="hidden" name="amount" value="10000" />
												<input type="hidden" name="currency" value="DKK" />
												<input type="hidden" name="orderid" value="<?php print($_SESSION["order"]); ?>" />
												<input type="hidden" name="decorator" value="responsive" />

												<input type="hidden" name="test" value="1" />
												<button class="btn btn-default btn-lg" type="submit">Køb nu</button>
												<p class="under-footer">Ved at klikke 'køb nu' acceptere du vores <a class="under-footer" href="termsofsale.html" target="_blank">handelsbetingelser</a></p>
											</form>
										</li>
									</ul>
								<?php elseif ($_SESSION["product"] == "extra"): ?>
									<?php if($_SESSION["permission"] == 1): ?>
										<?php Licens::generateOrderID(); Licens::generateLicens(); ?>
										<br><br><br>
										<h2>Kurv:</h2>
										<p>&bull; Forældrepakken, indenholder: tid- og kalenderfunktionen<br></p>

										<p>
											Ialt:<span class="invisible">lills</span>150 DKK
										</p>
										<ul class="list-inline banner-social-buttons">
											<li>
												<form method="post" action="https://payment.architrade.com/paymentweb/start.action" accept-charset="utf-8">
													<input type="hidden" name="merchant" value="90234185" />
													<input type="hidden" name="accepturl" value="https://www.focuslock.dk/pages/process?orderid=<?php print($_SESSION["order"]); ?>" />
													<input type="hidden" name="amount" value="15000" />
													<input type="hidden" name="currency" value="DKK" />
													<input type="hidden" name="orderid" value="<?php print($_SESSION["order"]); ?>" />
													<input type="hidden" name="decorator" value="responsive" />

													<input type="hidden" name="test" value="1" />
													<button class="btn btn-default btn-lg" type="submit">Køb nu</button>
													<p class="under-footer">Ved at klikke 'køb nu' acceptere du vores <a class="under-footer" href="termsofsale.html" target="_blank">handelsbetingelser</a></p>
												</form>
											</li>
										</ul>
									<?php elseif ($_SESSION["permission"] == 2): ?>
										<?php Licens::generateOrderID(); Licens::generateLicens(); ?>
										<br><br><br>
										<h2>Kurv:</h2>
										<p>&bull; Forældrepakken, indenholder: tid- og kalenderfunktionen<br></p>

										<p>
											Ialt:<span class="invisible">lills</span>50 DKK
										</p>
										<ul class="list-inline banner-social-buttons">
											<li>
												<form method="post" action="https://payment.architrade.com/paymentweb/start.action" accept-charset="utf-8">
													<input type="hidden" name="merchant" value="90234185" />
													<input type="hidden" name="accepturl" value="https://www.focuslock.dk/pages/process?orderid=<?php print($_SESSION["order"]); ?>" />
													<input type="hidden" name="amount" value="5000" />
													<input type="hidden" name="currency" value="DKK" />
													<input type="hidden" name="orderid" value="<?php print($_SESSION["order"]); ?>" />
													<input type="hidden" name="decorator" value="responsive" />

													<input type="hidden" name="test" value="1" />
													<button class="btn btn-default btn-lg" type="submit">Køb nu</button>
													<p class="under-footer">Ved at klikke 'køb nu' acceptere du vores <a class="under-footer" href="termsofsale.html" target="_blank">handelsbetingelser</a></p>
												</form>
											</li>
										</ul>
									<?php endif; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<?php include "../includes/footer.php" ?>

		</body>
	</html>
<?php else: ?>
	<script type="text/javascript">
		window.location.href = 'https://www.focuslock.dk';
	</script>
<?php endif; ?>
