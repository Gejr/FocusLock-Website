<?php
	require_once("../includes/init.php");

    //Pass af FocusLock program
    $email = $_GET['email'];
    $hashedpass = $_GET['pass'];

	if($email && $hashedpass !== null)
	{
		if(Auth::getInstance()->loginC($email, $hashedpass))
        {
        	try
            {
               	$db = Database::getInstance();
				$stmt = $db->prepare("SELECT * FROM users WHERE email = :email");
				$stmt->execute([':email' => $email]);
				$user = $stmt->fetchObject('User');

               	echo 'granted;', $user->permission, ';', $user->unitid, ';', $user->id, ';', $user->name;
			}
			catch (PDOException $exception)
            {
               	error_log($exception->getMessage());
               	return false;
            }
		}
		else
		{
			echo 'Login failed, email or password is incorrect.';
		}
	}
	else
	{
		echo 'Email or password is not entered.';
	}
