<?php
//require("Config.class.php");
//Database Class

class Database
{
	private static $_db;            			//Singleton connection objekt
	private function __construct() {}            //Tillader ikke at der laves en ny objekt af class'en med en ny database
	private function __clone() {}                   //Tillader ikke at klone class'en

	// Skaffer en instans af PDO forbindelsen
	
	public static function getInstance()
	{
		if(static::$_db === NULL)
		{
			$dsn = "mysql:host=" . Config::DB_HOST . ";dbname=" . Config::DB_NAME . ";charset=utf8";
			static::$_db = new PDO($dsn, Config::DB_USER, Config::DB_PASS);

			//Laver en exception når databasen laver en exception
			static::$_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		return static::$_db;
	}
}

?>