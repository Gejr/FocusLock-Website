<?php
	session_start();
    require_once("../includes/init.php")
?>
<html lang="da-DA">
        <?php include "../includes/header.php" ?>

        <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

        <?php include "../includes/navbar.php" ?>

		<header class="about">
			<div class="about-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1 class="brand-heading">Om FocusLock</h1>
							<h6 class="about-text">Hvad, hvem, hvorfor & hvordan?</h6>

							<div class="button-circle">
								<a href="#about" class="btn btn-circle page-scroll animated">
									<i class="fas fa-lock"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

        <div class="about-section" id="about">
            <div class="what-section">
            	<section id="what-is" class="container content-section text-center">
                    <div class="about-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="about-title">Hvad er FocusLock?</h1>
                                    <p class="about-text">FocusLock er skabt til at træne en i selvmotivation, fokus og selvdisciplin.
										<br>
										FocusLock er et program der kan lukke programmer ned og blokere hjemmesider.
										<br>
										FocusLock Lukker selvvalgte programmer og hjemmesider ned.
										<br>
										FocusLock er tilegnet privatpersonen der nemt bliver distraheret og har brug for hjælp med koncentration.
										<br>
										FocusLock er også til skoler, forældre og firmaer.
										<br>
										FocusLock variere mellem 3 forskellige versioner. Hver version lavet til en specifik målgruppe såsom
										skoler, forældre, firmaer og privat personer.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            	</section>
            </div>

			<div class="usage-section">
                <section id="about-usage" class="container content-section text-center">
                    <div class="about-usage-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="usage-title">Hvad kan FocusLock bruges til?</h1>
                                    <p class="usage-text">FocusLock kan bruges til træning af selvmotivation og selvdisciplin.
										Du kan selv vælge hvilke hjemmesider og programmer der vil blive lukket og blokeret i
										en selv given tidsperiode. Dette gør det muligt for brugeren at justere tiden og udvalget
										af hjemmesider og programmer, med det mål at de kan fokusere uden brugen af FocusLock.
										FocusLock kan bruges til at give forældre kontrol over deres børns computerforbrug. Forældre
										kan bestemme hvornår deres børn kan bruge computeren til spil, musik, social medier og meget
										mere. De kan lave tidsperioder hvor det kun er muligt at lave lektier på computeren. De får
										mulighed for at tilpasse en personlig liste med programmer og hjemmesider de føler deres børn
										bliver distraheret af når de skal lave lektier. De kan selvfølgelig også sætte programmer på
										“standby” som de føler deres børn bliver for opslugte af.
                                    	<br><br>
                                    	FocusLock kan hjælpe skoler med at få en bedre kontrol over elevernes brug af computere i
										undervisningen. Skoler vil være i stand til at lægge planer ind i FocusLock så man kan
										arrangere og planlægge ud i fremtiden. Der vil være mulighed for at lærerne kan ændre i de
										valgte programmer og hjemmesider i løbet af undervisningen hvis der er brug for det. Der bliver
										mulighed for at vælge elever fra og til på listen af computere hvor FocusLock skal køre. Dette
										giver eleverne mulighed for at arbejde med betydningen af “frihed under ansvar”.
                                	</p>
                                </div>
                            </div>
                        </div>
                    </div>
            	</section>
        	</div>

            <div class="us-section">
                <section id="about-us" class="container content-section text-center">
                    <div class="about-us-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="about-title">Hvem er vi?</h1>
                                    <p class="about-text">FocusLock er udviklet og skabt af unge der har oplevet problemer med deres
										koncentration gennem deres opvækst. Der er en fælles holdning omkring computerens påvirkning af problemet.
                                        Det udviklede sig derfor til skabelsen af et program der skulle hjælpe andre, så de ville få endnu bedre muligheder.
                                    	<br><br>
                                    	Projektet blev oprettet i <a class="hyperlink" target="_blank" href="http://www.sceu.dk/teknisk-gymnasium.dk">Slagelse
										gymnasiet</a> da vi stod med opgaven “Hvad skal Danmark leve af”. Vores første tanke var “uddannelse”,
										da Danmark er et land der går foran når det gælder udvikling og forbedring af den viden vi har.
                                    	Alligevel er Danmarks største svaghed den udvikling vi har skabt. Unge har svært ved at koncentrere
										sig, og der er mange der ikke kan følge med. For at løse dette problem lagde vi hovederne sammen og
										kom op med ideen FocusLock.
                                	</p>
                                </div>
                            </div>
                        </div>
                    </div>
            	</section>
        	</div>

            <div class="why-section">
                <section id="why-us" class="container content-section text-center">
                    <div class="why-us-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="about-title">Hvorfor & hvorfor os?</h1>
                                    <p class="about-text">Vi kender selv problemet med manglende koncentration. Der har været adskillige situationer
										hvor vi sagde til os selv “Nu skal jeg lave noget. Jeg har ikke tid til at slappe af på sofaen”. Alligevel endte
										det med det komplet modsatte, nemlig at slappe af på sofaen og ikke nå det der skulle være lavet. Det er ikke bare
										irriterende, men medføre også stress, og stress medfører til demotivation. Med andre ord ender man i en uendelig cirkel
										af ugidelighed. Det kan efterfølgende medføre til nedbrydning af ens selvværd. Det lyder måske meget overdrevet når
										man sætter det op på denne måde, problemet er bare at det fungerer på den måde.
                                        <br><br>
                                        Vi kan alle relatere til opgaver man aldrig får afleveret da man ikke kan få sig selv til at lave dem, når der er så meget
										andet man hellere vil. Det ligger sig bare som dårlig samvittighed der hænger over en i flere uger, og man bruger alt for
										meget tid på at tænke over det. Alt det kunne undgås hvis bare man fik lavet opgaven, men det er alligevel rigtig svært i
										sig selv. Derfor vil vi gerne hjælpe med at få fjernet nogle af alle de distraktioner der ligger på din computer.
                                    </p>
                                    <p class="about-text">
                                        Så hvorfor ikke vælge os?
                                        <br>
                                        Klik her for at downloade FocusLock.
                                    </p>
                                    <ul class="list-inline banner-social-buttons">
                                        <li>
                                            <a href="solutions" class="btn btn-default btn-lg"><i class="far fa-window-maximize fa-fw"></i> <span class="network-name">FocusLock</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                	</div>
                </section>
            </div>

			<div class="goals-section">
                <section id="about-goals" class="container content-section text-center">
                    <div class="about-goals-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="goals-title">Mål og ambitioner</h1>
                                    <p class="goals-text">get it now bruv!<br>inb4 the Five-O's closes us down!!1
                                	</p>
                                </div>
                            </div>
                        </div>
                    </div>
            	</section>
        	</div>

            <div class="how-section">
                <section id="how" class="container content-section text-center">
                    <div class="how-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="about-title">Hvordan fungerer FocusLock?</h1>
                                    <p class="about-text">FocusLock, er lavet så brugervenligt som overhovedt muligt. Alt hvad du skal er at klikke start, vi
										har klaret resten! Med programmet følger en lang liste med spil og andre programmer der kan og vil forstyre undervisningen.
										Så derfor skal du bare starte programmet, og så fungerer det bare!
                                        <br><br>
                                        Hvis du finder et program vi ikke har blokeret endnu, så kan du skrive til os på <a class="hyperlink" href="support">support
										linjen</a>, og vi tilføjer det for dig så næste gang programmet åbens er det også blokeret!
                                    </p>
                                </div>
                            </div>
                        </div>
                	</div>
                </section>
            </div>
    	</div>

        <?php include "../includes/footer.php" ?>

        </body>
</html>
