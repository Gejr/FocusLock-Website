<?php
	session_start();
    require_once("../includes/init.php")
?>
<html lang="da-DA">
        <?php include "../includes/header.php" ?>

        <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

        <?php include "../includes/navbar.php" ?>

        <!-- Intro container start -->

	<header class="solutions">
		<div class="solutions-body">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1 class="brand-heading">Produkter</h1>
                        <h6 class="solutions-text">
							<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-left"></i></span></sup>
							 Hvad tilbyder vi?
							 <sup><span class="fa-layers fa-fw"><i class="fas fa-quote-right"></i></span></sup>
						</h6>

                        <div class="button-circle">
							<a href="#solution" class="btn btn-circle page-scroll animated">
								<i class="fas fa-lock"></i>
							</a>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Intro container slut -->

        <div class="solution-section" id="solution">
            <div class="which-section">
                <section id="which" class="container content-section text-center">
                    <div class="which-focuslock">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="which-title">Hvilken version er den rette til mig?</h1>
                                    <p class="which-text" id="version1">Vi tilbyder <u>3</u> versioner af FocusLock.</p>
                                        <p class="which-text" > <br>
                                            <b>Første version</b> er rettet mod selvmotivation og selvdisiplin.
                                            Denne version er til folk der har brug for det ekstra skub til at hjælpe dem selv
                                            med at fokusere på lektierne, arbejdet eller den nuværende opgave.
                                            <a id="version2"> </a>
                                        </p >

                                        <p class="which-text"> <br>
                                            <b>Anden version</b> er til skolerne. Denne version er lavet til skolerne. Her har du mulighed
                                            for at starte programmet (trådløst) for den klasse du underviser. Nu kan du
                                            sørge for klassen ikke bliver forstyrret af spil og facebook.
                                            <a id="version3"> </a>
                                        </p>

                                        <p class="which-text"> <br>
                                            <b>Tredje version</b> er til forældre som har svært ved at sætte grænser heller svært ved at
                                            overholde dem. Du kan sørge for at dit barn ikke sidder og spiller til langt ud på
                                            natten en hverdag. Du har mulighed for at lave en kalender for at aktivere programmet
                                            på specifikke tidspunkter. Hvis du ikke vil have dit barn skal spille efter klokken 22,
                                            så kan du lave kalenderen så programmet bliver aktiveret efter 22. (SKAL OMFORMOLERES)
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="price-section">
                    <section id="price" class="container content-section text-center">
                        <div class="price-focuslock">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <h1 class="price-title">Pris</h1>
                                        <p class="price-text">
                                            Hvad koster FocusLock?
                                        </p>

                                        <p class="price-text">
                                            FocusLock har 3 forskellige pris nivuaer, vi er kendt med budgettet som studerne,
                                            så derfor tilbyder vi den <u><a href="#version1" class="page-scroll">selvmotiverende version</a></u> gratis.
                                        	<br>
                                            Hvis du bruger denne version og finder at den hjælper dig, og du har lidt ekstra
                                            mønter i lommen ville vi dog blive glade hvis du vil donere lidt til os.
                                        </p>

                                        <p class="price-text">
                                            <u><a href="#version3" class="page-scroll">Forældre versionen</a></u> koster penge. Denne version
                                            koster 100 DKK. for en licens.
                                        </p>

                                        <p class="price-text">
                                            <u><a href="#version2" class="page-scroll">Skole versionen</a></u> koster også penge, dog skal skoler
                                            kontakte os for at få tilbud specificeret til jeres skole. Vi vil henvise jer
                                        	til vores mail her: <a href="contact" class="hyperlink">Klik mig.</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            <div class="product-section">
                <section id="product" class="container content-section text-center">
                    <div class="product-focuslock">
                    	<div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h1 class="product-title">Download</h1>

									<ul class="list-inline banner-social-buttons">
				                        <li>
							        		<a href="signup" class="btn btn-default btn-lg"> <span class="network-name">Download nu</span></a>
						                </li>
			                    	</ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <?php include "../includes/footer.php"?>

        </body>
</html>
