<?php
	class onlineUser
	{
		public function addUserToDB($passedId)
		{
			if($passedId !== null)
			{
				try
				{
					$db = Database::getInstance();

					$stmt = $db->prepare('SELECT * FROM users WHERE id = :id LIMIT 1');
					$stmt->execute([':id' => $passedId]);
					$user = $stmt->fetchObject('User');

					// DEBUG:
					// print_r($user);
					//
					
					$data = new static();
					$data->id = $passedId;
					$data->email = $user->email;
					$data->telefone = $user->telefone;
				    $data->unitid = $user->unitid;

					try
			        {
			            $db = Database::getInstance();

			            $stmt = $db->prepare('INSERT INTO online (id, email, telefone, unitid) VALUES (:id, :email, :telefone, :unitid)');
						$stmt->bindParam(':id', $data->id);
						$stmt->bindParam(':email', filter_var($data->email, FILTER_SANITIZE_EMAIL));
						$stmt->bindParam(':telefone', $data->telefone);
			            $stmt->bindParam(':unitid', $data->unitid);

						$stmt->execute();
					}
					catch (PDOException $exception)
					{
						error_log($exception->getMessage());
						return false;
					}
				}
				catch (PDOException $exception)
				{
					error_log($exception->getMessage());
					return false;
				}
			}
			else
			{
				echo 'ID is not entered.';
			}
		}

		public function removeUserFromDB($passedId)
		{
			if($passedId !== null)
			{
				try
				{
					$db = Database::getInstance();

					$stmt = $db->prepare('DELETE FROM online WHERE id = :id LIMIT 1');
					$stmt->execute([':id' => $passedId]);
				}
				catch (PDOException $exception)
				{
					error_log($exception->getMessage());
					return false;
				}
			}
			else
			{
				echo 'ID is not entered.';
			}
		}

		public function object_to_array($data)
		{
		    if(is_array($data) || is_object($data))
		    {
		        $result = array();

		        foreach($data as $key => $value) {
		            $result[$key] = $this->object_to_array($value);
		        }

		        return $result;
		    }

		    return $data;
		}
	}
?>
