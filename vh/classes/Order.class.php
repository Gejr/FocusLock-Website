<?php
/**
 *	Works out the paymentID and makes sure it is unique
 */
class Order
{

	public function generatePaymentID()
	{
		$orderid = mt_rand(1, 999999);
		return $orderid;
	}




	public function checkPaymentID($passedOrderid)
	{
		try
        {
            $db = Database::getInstance();
            $stmt = $db->prepare('SELECT COUNT(*) FROM orders WHERE orderid = :orderid LIMIT 1');
            $stmt->execute([':orderid' => $passedOrderid]);

            $rowCount = $stmt->fetchColumn();
            if($rowCount != 0)
			{
				Order::rea();
			}
			else
			{
				return $passedOrderid;
			}
        }

        catch (PDOException $exception)
        {
            error_log($exception->getMessage());
        	return false;
        }
	}




	public function savePaymentID()
	{
		try
		{
			$db = Database::getInstance();

			$stmt = $db->prepare('INSERT INTO orders (orderid, type, amount) VALUES (:orderid, :type, :amount)');
			$stmt->bindParam(':orderid', $_SESSION['paymentID']);
			$stmt->bindParam(':type', $_SESSION['pelletType']);
			$stmt->bindParam(':amount', $_SESSION['Quantity']);

			$stmt->execute();
			return true;
		}
		catch (PDOException $exception)
		{
			error_log($exception->getMessage());
			return false;
		}
	}
}

?>
