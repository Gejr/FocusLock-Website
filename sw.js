var CACHE_NAME = 'focuslock-cache-v1';
var urlsToCache = [
	'/',
    '/pages/about.php',
    '/pages/contact.php',
    '/pages/login.php',
    '/pages/process.php',
    '/pages/profile.php',
    '/pages/signup.php',
    '/pages/signup_success.php',
    '/pages/solutions.php',
    '/pages/support.php',
  	'/css/main.css',
  	'/js/focuslock.js',
  	'/js/jquery-3.2.1.min.js',
  	'/js/jquery-easing.1.3.js',
  	'/js/jquery-easing.compatibility.js',
  	'/js/jquery-ui.min.js',
];

self.addEventListener('install', function(event) {
  // Perform install steps
  console.log('install');
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

// self.addEventListener('fetch', function(event) {
//   console.log('fetch');
//   event.respondWith(
//     caches.match(event.request)
//       .then(function(response) {
//         // Cache hit - return response
//         if (response) {
// 		  console.log('Cache hit - responding');
//           return response;
//         }
//         return fetch(event.request);
//       }
//     )
//   );
// });

self.addEventListener('fetch', function(event) {
  console.log('fetch');
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
		  console.log('Cache hit - responding');
          return response;
        }

        // IMPORTANT: Clone the request. A request is a stream and
        // can only be consumed once. Since we are consuming this
        // once by cache and once by the browser for fetch, we need
        // to clone the response.
        var fetchRequest = event.request.clone();

        return fetch(fetchRequest).then(
          function(response) {
            // Check if we received a valid response
            if(!response || response.status !== 200 || response.type !== 'basic') {
			  console.log('Respond is good');
              return response;
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.
            var responseToCache = response.clone();

            caches.open(CACHE_NAME)
              .then(function(cache) {
				console.log('Cache putting');
                cache.put(event.request, responseToCache);
              });

			console.log('Final respond');
            return response;
          }
        );
      })
    );
});
