<?php
require_once("../includes/init.php");

//Debugging
$debug = true;
if($debug == true)
{
	ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

//Pass af FocusLock program
$pPerm = $_GET['perm'];
$pMail = $_GET['mail'];
$pBool = $_GET['create'];
$pSecret = $_GET['secret'];
$pStamp = $_GET['stamp'];

$stamp = round(microtime(true));
if($stamp == $pStamp)
{
	if($debug)
	{
		echo "<br>Timestamps matched!";
	}
	if($pPerm != null)
	{
		if($pMail != null)
		{
			if($pBool != null)
			{
				if($pSecret != null)
				{
			        $secret = hash('sha256', $pMail . hash('sha256', $pMail, true), false);

					if(strtolower($secret) === strtolower($pSecret))
					{
						if($debug == true)
						{
							echo "<br>Secrets matched";
						}

								/*******************/
								/*   OPRET MAPPE   */
								/*******************/
						if($pBool === "true")
						{
							chdir("../ftp/FocusLock" . "/" . $pPerm . "/");
							if($debug == true)
							{
								echo getcwd();
							}
							if(!is_dir($pMail)) //Mappe ekistere ikke
							{
								if($debug == true)
								{
									echo "<br>Directory didn't exist, but is now created";
								}
								mkdir($pMail);
								chdir($pMail . "/");
								if($debug == true)
								{
									echo getcwd();
								}
								if(!file_exists($pMail.".txt"))  //Fil ekistere ikke
								{
									$handle = fopen($pMail.".txt", "c");
									fclose($handle);
									if($debug == true)
									{
										echo "<br>file didn't exist, but is now created";
									}
								}
								else //Fil ekistere
								{
									if($debug == true)
									{
										echo "<br>file exist, nothing has been created";
									}
								}
							}
							else {  //Mappe ekistere
								if($debug == true)
								{
									echo "<br>Directory existed, nothing was created";
								}
								chdir($pMail . "/");
								if($debug == true)
								{
									echo getcwd();
								}
								if(!file_exists($pMail.".txt")) //Fil ekistere ikke
								{
									$handle = fopen($pMail.".txt", "c");
									fclose($handle);
									if($debug == true)
									{
										echo "<br>file didn't exist, but is now created";
									}
								}
								else  //Fil ekistere
								{
									if($debug == true)
									{
										echo "<br>file exist, nothing has been created";
									}
								}
							}
							if($debug == true)
							{
								echo "<br><br>dir tree:<br>";
								$d = dir(".");
								while (false !== ($entry = $d->read())) {
									echo "<b>".$entry."</b><br>";
								}
								$d->close();
							}
						}

								/******************/
								/*   SLET MAPPE   */
								/******************/
						else if($pBool === "false")
						{
							chdir("../ftp/FocusLock" . "/" . $pPerm . "/" . $pMail);
							if($debug == true)
							{
								echo getcwd();
							}
							if(file_exists($pMail.".txt"))
							{
								unlink($pMail.".txt");
								if($debug == true)
								{
									echo "<br>file existed, but is now deleted";
								}
							}
							else
							{
								if($debug == true)
								{
									echo "<br>file didn't exist, nothing deleted.";
								}
							}
							if($debug == true)
							{
								echo "<br><br>dir tree:<br>";
								$d = dir(".");
								while (false !== ($entry = $d->read())) {
								   echo "<b>".$entry."</b><br>";
								}
								$d->close();
							}
						}
						else
						{
							if($debug == true)
							{
								echo "<br>pBool is wrong!";
							}
						}
					}
					else
					{
						if($debug == true)
						{
							echo "<br>secrets did not match!";
							echo "<br>web :".strtoupper($secret);
							echo "<br>proc :".strtoupper($pSecret);
						}
					}
				}
				else
				{
					if($debug == true)
					{
						echo "<br>pSecret was not passed!";
					}
				}
			}
			else
			{
				if($debug == true)
				{
					echo "<br>pBool was not passed!";
				}
			}
		}
		else
		{
			if($debug == true)
			{
				echo "<br>pMail was not passed!";
			}
		}
	}
	else
	{
		if($debug == true)
		{
			echo "<br>pPerm was not passed!";
		}
	}
}
else
{
	if($debug == true)
	{
		echo "<br>Time stamps did not match!";
		echo "<br>web :".$pStamp;
		echo "<br>proc  :".$stamp;
	}
}
?>
<title>ftp-file-handling</title>
