<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand page-scroll" href="http://www.focuslock.dk">
				<i class="fa fa-arrow-left" aria-hidden="true"></i> <b> Tilbage til FocusLock</b>
			</a>
		</div>

		<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			<a class="navbar-brand page-scroll" href="../pages/signup">
				<b>Ikke oprettet? </b> <i class="fa fa-arrow-right" aria-hidden="true"></i>
			</a>
		</div>
	</div>
</nav>
