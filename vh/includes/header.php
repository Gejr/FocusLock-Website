<head>
	<title>VH-Piller</title>

	<!-- META TAGS -->
	   	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="xxx">
	    <meta name="keywords" content="xxx">
	    <meta name="robots" content="index, follow">
	    <meta name="revisit-after" content="10 days">
	    <meta name="author" content="Malte Gejr">
	    <link rel="author" href="https://plus.google.com/u/0/109421041800839032865">
	    <link rel="me" href="https://plus.google.com/u/0/109421041800839032865">
	<!-- /META TAGS -->

	<!-- REL LINKS -->
        <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui.structure.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui.theme.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.css.map" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.css.map" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css.map" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="css/vh.css" rel="stylesheet" type="text/css">

		<link href="https://fonts.googleapis.com/css?family=Ubuntu|Raleway" rel="stylesheet"> 
        <link href="images/favicon.ico" rel="icon">
	<!-- /REL LINKS -->

	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
