<?php
require_once("../includes/init.php");

$passedOrderId = $_GET['orderid'];
$passedLicens = $_SESSION['licens'];

if($passedOrderId != null)
{
	// checks if the orderids are the same.
	if($_SESSION['order'] == $passedOrderId)
	{
		Licens::updatePerm();
		Licens::saveLicens();
		// print_r($_SESSION['licens']. "<br>");
		// print_r("orderid er det samme <br>");
	}
	else
	{
		echo "<script>
				alert('Der var noget der gik galt, prøv venligst igen. Hvis denne fejl bliver ved med at opstå kontakt venligst FocusLock.\nError code: 02x2');

				window.location.href = 'https://www.focuslock.dk/pages/profile';
			  </script>";
	}
}
else
{
	echo "<script>
			alert('Der var noget der gik galt, prøv venligst igen. Hvis denne fejl bliver ved med at opstå kontakt venligst FocusLock.\n6Error code: 02x1');

			window.location.href = 'https://www.focuslock.dk/pages/profile';
		  </script>";
}
?>
<html lang="da-DA">
	<?php include "../includes/header.php" ?>
	<body id="page-top" data-spy="scroll" style="overflow: hidden;">
		<nav class="navbar" role="navigation"></nav>

		<div class="text-center" style="margin: 200 0 0 0">
			<span class="working">
				<h1>Behandler din ordre</h1>
				<span class="fa-layers fa-fw fa-6x">
					<i class="fas fa-spinner-third fl-rotate-1x"></i>
					<i class="far fa-spinner-third fl-rotate-2x" data-fa-transform="shrink-5 rotate-120"></i>
					<i class="fal fa-spinner-third fl-rotate-3x" data-fa-transform="shrink-8 rotate-240"></i>
				</span>
				<br><br>
				<p class="info-text">Dette kan tage et par minutter,<br>venligst undgå at lukke siden eller webbrowseren.</p>
			</span>
		</div>
		<div class="text-center" style="margin: -225 0 0 0">
			<span class="done invisible">
				<h1>Din konto er nu opgraderet!</h1>
				<p>
					Din konto er nu blevet opdateret, og du kan nu åbne FocusLock igen og bruge de nye funktioner!
					<br><br>
					<span class="windowsOnly">
					Hvis du allerede har downloadet FocusLock kan du blot lukke denne side nu.
					<br>
					Hvis du endnu ikke har downloadet FocusLock klik da her:<span class="invisible">lil</span><a href="https://www.focuslock.dk/download/focuslock-installer.exe" target="_blank" class="btn btn-default btn-lg"><span class="network-name">Download</span></a>
					</span>
				</p>
				<p class="info-text">Du vil automatisk blive sendt tilbage om 15 sekunder</p>
			</span>
		</div>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-98982854-1', 'auto');
			ga('send', 'pageview');
		</script>
		<!-- REL -->
		<script src="https://www.focuslock.dk/js/jquery-3.2.1.js"></script>
		<script src="https://www.focuslock.dk/js/jquery-3.2.1.min.js"></script>
		<script src="https://www.focuslock.dk/js/jquery-easing.1.3.js"></script>
		<script src="https://www.focuslock.dk/js/jquery.easing.min.js"></script>
		<script src="https://www.focuslock.dk/js/jquery-easing.compatibility.js"></script>
		<script src="https://www.focuslock.dk/js/jquery.js"></script>
		<script src="https://www.focuslock.dk/js/jquery-ui.js"></script>
		<script src="https://www.focuslock.dk/js/jquery-ui.min.js"></script>
		<script src="https://www.focuslock.dk/js/bootstrap.js"></script>
		<script src="https://www.focuslock.dk/js/bootstrap.min.js"></script>
		<script src="https://www.focuslock.dk/js/focuslock.js"></script>
		<script src="https://www.focuslock.dk/js/npm.js"></script>
		<!-- /REL -->

		<script>
			$(document).ready(function()
			{
				if(/Linux|UNIX|MacOS|Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
				{
					$(".windowsOnly").addClass("invisible");
				}

				var x = getRndInteger(2500, 6000);

				window.setTimeout(function()
				{
					$(".working").addClass("invisible");
					$(".done").removeClass("invisible");
				}, x);

				window.setTimeout(function()
				{
					location.href = "https://www.focuslock.dk/pages/profile";
				}, x + 15000);
			});

			function getRndInteger(min, max) {
			    return Math.floor(Math.random() * (max - min + 1) ) + min;
			}
		</script>
	</body>
</html>
