<?php
// Auto Class Loader
spl_autoload_register("myAutoloader");

function myAutoloader($className)
{
        require dirname(dirname(__FILE__)) . "/classes/" . $className . ".class.php";
}

Auth::init();

preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
if(count($matches)<2){
  	preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
}

if (count($matches)>1)
{
  	//Then we're using IE
  	$version = $matches[1];
	Util::Redirect("/error_paths/IE.html");

	
	// switch(true)
	// {
    // 	case ($version<=8):
    // 		Util::Redirect("/error_paths/IE.html");
    // 		break;
	//
    // 	case ($version==9 || $version==10):
    //   		Util::Redirect("/error_paths/IE.html");
    //   		break;
	//
    // 	case ($version==11):
    //   		Util::Redirect("/error_paths/IE.html");
    //   		break;
	//
    // 	default:
    // 		Util::Redirect("/error_paths/IE.html");
	// }
}

?>
