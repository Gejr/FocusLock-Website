<?php

//Auth class

class Auth
{
    private static $_instance;                          //Singleton instans
    private $_currentUser;                              //Logged ind bruger
    private function __construct() {}            		//Tillader ikke at der laves en ny objekt af class'en med en ny Auth()
	private function __clone() {}                   //Tillader ikke at klone class'en

    public static function init()
    {
        session_start();
    }

    public static function getInstance()
    {
        if(static::$_instance === NULL)
        {
            static::$_instance = new Auth();
        }

        return static::$_instance;
    }

    //Login bruger
    public function login($email, $password)
    {
        $user = User::authenticate($email, $password);

        if($user !== null)
        {
            $this->_currentUser = $user;

            //Gemmer brugeren ID i session
			$_SESSION["user_id"] = $user->id;
			$_SESSION["name"] = $user->name;
			$_SESSION["email"] = $user->email;
			$_SESSION["telefone"] = $user->telefone;
			$_SESSION["permission"] = $user->permission;
			$_SESSION["unitid"] = $user->unitid;

            //Genere et Session ID for at forhindre  session hijacking
            session_regenerate_id();

            return true;
        }

        return false;
    }

	//Login bruger
    public function loginC($email, $hashedpass)
    {
    	$user = User::authenticateC($email, $hashedpass);

        if($user !== null)
        {
            $this->_currentUser = $user;

            //Gemmer brugeren ID i session
			$_SESSION["user_id"] = $user->id;
			$_SESSION["name"] = $user->name;
			$_SESSION["email"] = $user->email;
			$_SESSION["telefone"] = $user->telefone;
			$_SESSION["permission"] = $user->permission;
			$_SESSION["unitid"] = $user->unitid;

            //Genere et Session ID for at forhindre  session hijacking
            session_regenerate_id();

            return true;
        }

        return false;
    }

    public function getCurrentUser()
    {
        if($this->_currentUser === null)
        {
            if(isset($_SESSION["user_id"]))
            {
                //Cacher objektet så man kan i en enkel requets loade data fra databasen en  gang
                $this->_currentUser = User::findByID($_SESSION["user_id"]);
            }
        }

        return $this->currentUser;
    }

    public function isLoggedIn()
    {
		if($_SESSION["user_id"] !== null)
		{
			return true;
		}
		else
		{
			return $this->getCurrentUser() !== null;
		}
    }
}

?>
