<footer>
	<hr class="top-hr">
	<div class="fod contaier">
		<div class="container">
			<div class="row">
		    	<div class="col-md-4 offset">
		    		<h1 class="foot-header">Kontakt</h1>
					<hr class="foot-hr">

					<div class="kon">
						<div class="kon-name">
							<i class="fas fa-user-circle" style="color:#272727"></i> <a class="mainPoint">CVR:</a><br>
							<a class="invisible">...</a><a class="secPoint">BeGeBa CVR: 38834614</a>
						</div>
						<div class="kon-address">
							<i class="fas fa-home" style="color:#272727"></i> <a class="mainPoint">Adresse:</a><br>
							<a class="invisible">...</a><a class="secPoint">Enghaven 11, 4281 Gørlev</a>
						</div>
						<div class="kon-email">
							<i class="fas fa-envelope" style="color:#272727"></i> <a class="mainPoint">E-Mail:</a><br>
							<a class="invisible">...</a><a class="secPoint clickable" href="mailto:ge@focuslock.dk">ge@focuslock.dk</a>
						</div>
						<div class="kon-phone">
							<i class="fas fa-phone" style="color:#272727"></i> <a class="mainPoint">Telefon:</a><br>
							<a class="invisible">...</a><a class="secPoint clickable" href="tel:+4542243903">+45 4224 3903</a>
						</div>
					</div>
		    	</div>
		    	<div class="col-md-4 offset">
		    		<h1 class="foot-header">Information</h1>
					<hr class="foot-hr">

						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/termsofuse.html" target="_blank"> Vilkår & betingelser</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/termsofsale.html" target="_blank"> Handelsbetingelser</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/privacypolicy.html" target="_blank"> Fortrolighedspolitik</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/fortydelsesformular.pdf" target="_blank"> Returret</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/copyright.txt" target="_blank"> Ophavsret</a><br>
		    	</div>
		    	<div class="col-md-4 offset">
		    		<h1 class="foot-header">Brugbar links</h1>
					<hr class="foot-hr">

						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk"> Hjem</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/about"> Om os</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/solutions"> Produkter</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/pages/profile"> Bruger</a><br>
						<i class="fas fa-chevron-circle-right" style="color:rgba(39, 39, 39, 0.8);"></i><a class="secPoint clickable" href="https://www.focuslock.dk/sitemap.xml" target="_blank"> Sitemap</a><br>
		    	</div>
			</div>
		</div>

		<div class="under-footer container text-center">
	    	<p class="under-footer">&bull; <a class="under-footer nonhover" href="https://www.focuslock.dk/pages/copyright.txt">Copyright 2017 FocusLock</a> &bull; <a class="under-footer nonhover" href="#">Website created by Gejr[s]</a> &bull;</p>
			<div class="dibs_brand_assets" style="margin: 5 0 25px;">
				<img src="https://cdn.dibspayment.com/logo/checkout/combo/horiz/DIBS_checkout_kombo_horizontal_01.png" alt="DIBS - Payments made easy" width="380"/>
			</div>
	    </div>
	</div>
</footer>

<script type="text/javascript">
	if ('serviceWorker' in navigator)
	{
		window.addEventListener('load', function()
		{
			navigator.serviceWorker.register('/sw.js').then(function(registration)
			{
				// Registration was successful
				console.log('ServiceWorker registration successful with scope: ', registration.scope);
			}, function(err)
			{
				// registration failed :(
				console.log('ServiceWorker registration failed: ', err);
			});
		});
	}
</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-98982854-1', 'auto');
	ga('send', 'pageview');
</script>
<!-- REL -->
<script src="https://www.focuslock.dk/js/jquery-3.2.1.js"></script>
<script src="https://www.focuslock.dk/js/jquery-3.2.1.min.js"></script>
<script src="https://www.focuslock.dk/js/jquery-easing.1.3.js"></script>
<script src="https://www.focuslock.dk/js/jquery.easing.min.js"></script>
<script src="https://www.focuslock.dk/js/jquery-easing.compatibility.js"></script>
<script src="https://www.focuslock.dk/js/jquery.js"></script>
<script src="https://www.focuslock.dk/js/jquery-ui.js"></script>
<script src="https://www.focuslock.dk/js/jquery-ui.min.js"></script>
<script src="https://www.focuslock.dk/js/bootstrap.js"></script>
<script src="https://www.focuslock.dk/js/bootstrap.min.js"></script>
<script src="https://www.focuslock.dk/js/focuslock.js"></script>
<script src="https://www.focuslock.dk/js/npm.js"></script>
<!-- /REL -->
