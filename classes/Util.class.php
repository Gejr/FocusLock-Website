<?php
class Util
{
    public static function redirect($url)
    {
    	header('Location: https://' . $_SERVER['HTTP_HOST'] . $url);
        exit;
    }

	public function errorMail($errors)
	{
		mail ("begeba@focuslock.dk", "FEJL I ORDERID!", "Der er nu sket $errors fejl ved at fremskaffe et unikt id.\nVenligst tjek om der skal udvides fra mere end 6 cifret orderid'er eller om dette er rent tilfældigheder.", "\n\n~Util.class.php - Focuslock.dk");
	}
}
?>
