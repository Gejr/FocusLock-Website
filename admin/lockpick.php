<?php

    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);

	require_once("../includes/init.php");

	if($_SERVER['REQUEST_METHOD'] === 'POST')
	{
	    Auth::getInstance()->login($_POST['email'], $_POST['password']);
	}
?>
<!DOCTYPE html>
<body>
	<?php if (Auth::getInstance()->isLoggedIn()): ?>
		<?php if ($_SESSION["permission"] == 5): ?>
<!--

														░░░░░░░░░░░░░░░░░░░░░
														░░░░░░░░░░░░░▄▐░░░░░░
														░░░░░░░▄▄▄░░▄██▄░░░░░
														░░░░░░▐▀█▀▌░░░░▀█▄░░░
														░░░░░░▐█▄█▌░░░░░░▀█▄░
														░░░░░░░▀▄▀░░░▄▄▄▄▄▀▀░
														░░░░░▄▄▄██▀▀▀▀░░░░░░░
														░░░░█▀▄▄▄█░▀▀░░░░░░░░
														░░░░▌░▄▄▄▐▌▀▀▀░░░░░░░
														░▄░▐░░░▄▄░█░▀▀░░░░░░░
														░▀█▌░░░▄░▀█▀░▀░░░░░░░
														░░░░░░░░▄▄▐▌▄▄░░░░░░░
														░░░░░░░░▀███▀█░▄░░░░░
														░░░░░░░▐▌▀▄▀▄▀▐▄░░░░░
														░░░░░░░▐▀░░░░░░▐▌░░░░
														░░░░░░░█░░░░░░░░█░░░░
														░░░░░░▐▌░░░░░░░░░█░░░
														░░░░░░░░░░░░░░░░░░░░░

						   ▄████████    ▄███████▄  ▄██████▄   ▄██████▄     ▄█   ▄█▄    ▄████████ ████████▄
						  ███    ███   ███    ███ ███    ███ ███    ███   ███ ▄███▀   ███    ███ ███   ▀███
						  ███    █▀    ███    ███ ███    ███ ███    ███   ███▐██▀     ███    █▀  ███    ███
						  ███          ███    ███ ███    ███ ███    ███  ▄█████▀     ▄███▄▄▄     ███    ███
						▀███████████ ▀█████████▀  ███    ███ ███    ███ ▀▀█████▄    ▀▀███▀▀▀     ███    ███
							     ███   ███        ███    ███ ███    ███   ███▐██▄     ███    █▄  ███    ███
						   ▄█    ███   ███        ███    ███ ███    ███   ███ ▀███▄   ███    ███ ███   ▄███
						 ▄████████▀   ▄████▀       ▀██████▀   ▀██████▀    ███   ▀█▀   ██████████ ████████▀
																		  ▀
-->
			<?php include "../includes/header.php" ?>
			<style>
				table, th, td {
				    border: 1px solid black;
				    border-collapse: collapse;
					padding: 0px 5px 0px 5px;
					font-weight: normal;
					text-align: center;
				}
				button {
					border: 1px solid #C0C0C0;
					border-radius: 0;
				    text-transform: uppercase;
				    font-family: var(--main-font);
				    font-weight: 400;
				    -webkit-transition: all .3s ease-in-out;
				    -moz-transition: all .3s ease-in-out;
				    transition: all .3s ease-in-out;
					-moz-border-radius:5px;
					-webkit-border-radius:5px;
					border-radius:5px;
				}
				span {
					font-weight: bold;
				}
			</style>
			<script>
				function killdddClick()
				{
					// // var num = id;
					// // var num = 1;
					// // var str = stringInject("../php/removeUser.php?id={id}", { id: "1" });
					// if (!String.prototype.format) {
					//   String.prototype.format = function() {
					//     var args = arguments;
					//     return this.replace(/{(\d+)}/g, function(match, number) {
					//       return typeof args[number] != 'undefined'
					//         ? args[number]
					//         : match
					//       ;
					//     });
					//   };
					// }
					// var num = '<?php echo $var ?>' ;
					//
					//
					// window.open('{0}{1}'.format("../php/removeUser.php?id=", num), '_blank');

					// then echo it into the js/html stream
					// and assign to a js variable
					var json = JSON.parse(<?php echo stripslashes($_POST['$encoded']) ?>);
					// then
					// document.write(json_obj);
					alert(json);
				}
			</script>

			<?php
				if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['killClick']))
				{
					$passedId = $_POST['ID'];

					try
					{
						$db = Database::getInstance();

						$stmt = $db->prepare('DELETE FROM online WHERE id = :id LIMIT 1');
						$stmt->execute([':id' => $passedId]);
					}
					catch (PDOException $exception)
					{
						error_log($exception->getMessage());
						return false;
					}
				}
			?>

		<p><?php echo htmlspecialchars($_SESSION["name"]); ?></p>

		<?php
		$stack = array();
			for($id = 1; $id <= 20; $id++)
			{
				$db = Database::getInstance();

	            $stmt = $db->prepare('SELECT * FROM online WHERE id = :id LIMIT 1');
	            $stmt->execute([':id' => $id]);
	            $user = $stmt->fetchObject('User');
				array_push($stack, $user);
			}
		?>

		<div class="logged">
			<table>
				<thead><tr><th colspan="4"><span>Online right now</span></th></tr><tr><th colspan="4"> </th></tr><tr><th><span>ID</span></th><th><span>E-Mail</span></th><th><span>Telefone</span></th><th><span>Unit ID</span></th></tr></thead>
				<tbody>
					<?php foreach ($stack as $num) : ?>
						<?php if($num != null) : ?>
							<tr>
								<th><?= htmlspecialchars($num->id)?></th>
								<th><?=htmlspecialchars($num->email) ?></th>
								<th><?=htmlspecialchars($num->telefone) ?></th>
								<th><?=htmlspecialchars($num->unitid) ?></th>
								<th><form action="lockpick.php" method="post">
									<input type="submit" name="killClick" value="Kill"/>
									<input type="hidden" name="ID" value="<?= $num->id ?>"/>
								</form></th>
							</tr>
						<?php endif ?>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
		<?php else: ?>
			<script type="text/javascript">
				window.location.href = 'https://www.focuslock.dk';
			</script>
		<?php endif; ?>
	<?php else: ?>

<!--
									,---.
								   /    |
								  /     |
		  You shall not pass!    /      |
								/       |
		           \       ___,'        |
				         <  -'          :
		                  `-.__..--'``-,_\_
		                     |o/ <o>` :,.)_`>
		                     :/ `     ||/)
		                     (_.).__,-` |\
		                    /( `.``   `| :
		                    \'`-.)  `  ; ;
		                     | `       /-<
		                     |     `  /   `.
		     ,-_-..____     /|  `    :__..-'\
		    /,'-.__\\  ``-./ :`      ;       \
		    `\ `\  `\\  \ :  (   `  /  ,   `. \
		      \` \   \\   |  | `   :  :     .\ \
		       \ `\_  ))  :  ;     |  |      ): :
		      (`-.-'\ ||  |\ \   ` ;  ;       | |
		       \-_   `;;._   ( `  /  /_       | |
		        `-.-.// ,'`-._\__/_,'         ; |
		           \:: :     /     `     ,   /  |
		            || |    (        ,' /   /   |
		            ||                ,'   /    |

-->

		<div class="login">
			<form method="POST" class="formular">
				<div>
					<label for="email">E-Mail addresse</label>
					<input id="email" name="email" value="<?php echo isset($email) ? htmlspecialchars($email) : ''; ?>" readonly type="email" onfocus="if (this.hasAttribute('readonly')){this.removeAttribute('readonly');this.blur();this.focus();}"/>
				</div>

				<div>
					<label for="password">Kodeord</label>
					<input type="password" id="password" name="password" readonly onfocus="if (this.hasAttribute('readonly')){this.removeAttribute('readonly');this.blur();this.focus();}"/>
				</div>
				<br><br>
				<input type="submit" value="Log ind"/>
			</form>
		</div>
	<?php endif; ?>
</body>
