<footer class="site-footer">
	<div class="upper-footer container text-center">
		<a href="#" class="facebookBtn mdBtn"></a><a href="#" class="twitterBtn mdBtn"></a><a href="#" class="instagramBtn mdBtn"></a><br>
		<div class="align-mid">
			<div class="foot-col col-md-3">
				<img src="images/foot-logo" alt="VH-Piller" height="256" width="200">
			</div>
			<div class="foot-col col-md-3">
				<h1 class="foot-header">Information</h1>
				<hr class="foot-hr">
				<p>
					<a class="fa fa-arrow-circle-right" href="#"> Vilkår & Betingelser</a><br>
					<a class="fa fa-arrow-circle-right" href="#"> Fortrolighedspolitik</a><br>
					<a class="fa fa-arrow-circle-right" href="#"> Returret</a><br>
					<a class="invisible" href="#">Returret</a><br>
				</p>
			</div>
			<div class="foot-col col-md-3">
				<h1 class="foot-header">Brugbar links</h1>
				<hr class="foot-hr">
				<p>
					<a class="fa fa-arrow-circle-right" href="#"> Hjem</a><br>
					<a class="fa fa-arrow-circle-right" href="#"> Om os</a><br>
					<a class="fa fa-arrow-circle-right" href="#"> Produkter</a><br>
					<a class="fa fa-arrow-circle-right" href="#"> Bestil</a><br>
					<a class="fa fa-arrow-circle-right" href="#"> Sitemap</a><br>
				</p>
			</div>
			<div class="foot-col contact-col col-md-3">
				<h1 class="foot-header">Kontakt</h1>
				<hr class="foot-hr">
				<div class="contact">
					<div class="contact-name">
						<i class="fa fa-user-circle"> CVR:</i><br>
						<a class="invisible">...</a><a class="fa">VH-Piller CVR: XXXXXXX</a>
					</div>
					<div class="contact-address">
						<i class="fa fa-home"> Adresse:</i><br>
						<a class="invisible">...</a><a class="fa">Enghaven 11, 4281 Gørlev</a>
					</div>
					<div class="contact-email">
						<i class="fa fa-envelope"> E-Mail:</i><br>
						<a class="invisible">...</a><a class="fa" href="mailto:vh@vh-piller.dk">vh@vh-piller.dk</a>
					</div>
					<div class="contact-phone">
						<i class="fa fa-phone"> Telefon:</i><br>
						<a class="invisible">...</a><a class="fa" href="tel:+4542243903">+45 4224 3903</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="under-footer container text-center">
    	<p>&bull; <a href="copyright.txt">Copyright &copy; 2017 VH-Piller</a> &bull; <a href="#">Website created by Gejr IT</a> &bull;</p>
		<div class="dibs_brand_assets" style="margin: 5 0 25px;">
			<img src="https://cdn.dibspayment.com/logo/checkout/combo/horiz/DIBS_checkout_kombo_horizontal_01.png" alt="DIBS - Payments made easy" width="380"/>
		</div>
    </div>
</footer>

<script>
	// GOOGLE ANALYTICS SCRIPT
</script>

<!-- REL LINKS -->
<!--
	IMPORTANT ALL JQUERY MUST BE BEFORE BOOTSTRAP!!
	ELSE JQUERY WILL NOT WORK!!!
-->
<script src="js/jquery-3.2.1.js"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery-easing.1.3.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery-easing.compatibility.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/npm.js"></script>
<script src="js/vh.js"></script>
<script src="https://test.checkout.dibspayment.eu/v1/checkout.js?v=1"></script>
<!-- /REL LINKS -->
