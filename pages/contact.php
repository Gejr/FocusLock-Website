<?php
	session_start();
    require_once("../includes/init.php")
?>
<html lang="da-DA">
	<?php include "../includes/header.php" ?>

	<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include "../includes/navbar.php" ?>

	<header class="contact">
		<div class="contact-body">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1 class="brand-heading">Kontakt os</h1>
                    	<h6 class="contact-text">
							<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-left"></i></span></sup>
							Få fat i os hvor du vil, når du vil
							<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-right"></i></span></sup>
						</h6>

				        <div class="button-circle">
							<a href="#contact" class="btn btn-circle page-scroll animated">
								<i class="fas fa-lock"></i>
							</a>
				        </div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Intro container slut -->

	<div class="contact-section" id="contact">
	        <div class="why-contact-section">
		        <section id="why-contact" class="container content-section text-center">
		                <div class="why-contact-focuslock">
			                <div class="container">
			                        <div class="row">
				                        <div class="col-md-8 col-md-offset-2">
				                                <h1 class="why-contact-title">Hvorfor kontakt os?</h1>
				                                <p class="why-contact-text">
							                        Hvis du er en skole, så har vi specielle tilbud til dig.
							                        Vi kan tilbyde fordrag for din ledelse, vi tilbyder opsætning,
							                        support og specielle tilbud.
				                                </p>
				                        </div>
			                        </div>
			                </div>
		                </div>
		        </section>
	        </div>

	        <div class="contact-section">
		        <section id="contact" class="container content-section text-center">
		                <div class="contact-focuslock">
			                <div class="container">
			                        <div class="row">
				                        <div class="col-md-8 col-md-offset-2">
				                                <h1 class="contact-title">Kontakt os?</h1>
				                                <p class="contact-text">
					                        Vi har flere måder, du kan komme i kontakt den nemmeste er;
				                                </p>

				                                <p class="contact-text">
				                                Mail: du kan skrive til os når som helst og vi vil vende tilbage  så hurtigt som muligt.
				                                <br>
				                                <a href="mailto:be@focuslock.dk" target="_top"><u>be@focuslock.dk</u>  &mdash;  Sebastian Berg Rasmussen</a><br>
				                                <a href="mailto:ge@focuslock.dk" target="_top"><u>ge@focuslock.dk</u>  &mdash;  Malte Gejr Korup</a><br>
				                                <a href="mailto:ba@focuslock.dk" target="_top"><u>ba@focuslock.dk</u>  &mdash;  Sigurd Bang Mortensen</a><br><br>
												Kontakt addresse: Enghaven 11, 4281 Gørlev.
				                                </p>

				                                <p class="contact-text">
					                        Telefon: du kan altid ringe til os i tidsrummet 9 til 18, kvit og frit.
					                        <br>
					                        <a href="tel:+4529870788" target="_top"><u>+45 29 87 07 88</u> &mdash; Sebastian Berg Rasmussen</a><br>
					                        <a href="tel:+4542243903" target="_top"><u>+45 42 24 39 03</u> &mdash; Malte Gejr Korup</a><br>
					                        <a href="tel:+4553561222" target="_top"><u>+45 53 56 12 22</u> &mdash; Sigurd Bang Mortensen</a><br>
					                        <br>
					                        I kan også ringe uden for det frie tidsrum, men der kan blive pålagt gebyrer.
				                                </p>
				                        </div>
			                        </div>
			                </div>
		                </div>
		        </section>
	        </div>
        </div>

	<?php include "../includes/footer.php"?>

        </body>
</html>
