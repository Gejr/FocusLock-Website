<?php
	session_start();
	require_once("includes/init.php");
?>
<?php

	if(isset($_POST['6mm']))
	{
		$_SESSION['paymentID'] = Order::generatePaymentID();
		$_SESSION['pelletType'] = "6mm";
		$_SESSION['6mmQuantity'] = $_POST['quantity1'];
		$_SESSION['8mmQuantity'] = 0;
		$_SESSION['Quantity'] = $_POST['quantity1'];
		Util::redirect("/checkout.php");
	}

	if(isset($_POST['8mm']))
	{
		$_SESSION['paymentID'] = Order::generatePaymentID();
		$_SESSION['pelletType'] = "8mm";
		$_SESSION['6mmQuantity'] = 0;
		$_SESSION['8mmQuantity'] = $_POST['quantity2'];
		$_SESSION['Quantity'] = $_POST['quantity2'];
		Util::redirect("/checkout.php");
	}

?>

<html lang="da-DK">
	<?php include "includes/header.php" ?>

	<body id="page-top" data-spy"scroll" data-target=".navbar-fixed-top">
		<!-- Include Navbar her -->
		<header class="intro">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1 class="intro-heading">VH-Piller</h1>
							<h2 class="intro-text">Vi piller ved dig (¬‿¬)</h2>
						</div>
					</div>
				</div>
			</div>
		</header>

		<section id="buy">
			<div class="body">
				<div class="container content-section text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1 class="heading">Piller</h1>
							<h1 class="subheading">"Højkvalitets piller"</h1>
							<img src="images/wood_pellets" alt="VH-Piller" width="736">
							<div class="twoColumns">
								<div class="column1">
									<h3>6 mm</h3>
									<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
										reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
										in culpa qui officia deserunt mollit anim id est laborum.
									</p>

									<form method="POST">
										<ul class="text-center list-inline basket">
											<div class="quantity">
			  									<input name="quantity1" type="number" min="0" max="50" step="1" value="0">
												<li>
													<button id="submit" name="6mm" type="submit" class="btn btn-default btn-lg">
														<i class="fa fa-shopping-basket fa-fw"></i><span class="network-name">Tilføj til kurv</span>
													</button>
												</li>
											</div>
										</ul>
									</form>
								</div>

								<div class="column2">
									<h3>8 mm</h3>
									<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
										reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
										in culpa qui officia deserunt mollit anim id est laborum.
									</p>

									<form method="POST">
										<ul class="text-center list-inline basket">
											<div class="quantity">
		  										<input name="quantity2" type="number" min="0" max="50" step="1" value="0">
												<li>
													<button id="submit" name="8mm" type="submit" class="btn btn-default btn-lg">
														<i class="fa fa-shopping-basket fa-fw"></i><span class="network-name">Tilføj til kurv</span>
													</button>
												</li>
											</div>
										</ul>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Include Footer her -->
		<?php include "includes/footer.php" ?>
	</body>
</html>
