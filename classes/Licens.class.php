<?php

// Licens class

class Licens
{
	public $saveIsDone = false;
	public $updateIsDone = false;

	public function generateOrderID()
	{
		$orderid = mt_rand(1, 999999);
		Licens::checkOrderID($orderid);
	}

	public function generateLicens()
	{
		$chars = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E',
					   'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
					   'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		$licens = '';
		$max = count($chars)-1;
		for ($i = 0; $i < 25; $i++)
		{
 			$licens .= (!($i % 5) && $i ? '-' : '').$chars[rand(0, $max)];
  		}

		Licens::checkLicens($licens);
	}

	public function checkOrderID($passedOrderId)
	{
		if($passedOrderId !== null)
		{
			try
			{
				$db = Database::getInstance();

				$stmt = $db->prepare('SELECT * FROM licens WHERE orderid = :id LIMIT 1');
				$stmt->execute([':id' => $passedOrderId]);
				$licens = $stmt->fetchObject('User');

				if($licens->orderid !== null)
				{
					Licens::generateOrderID();
				}
				else
				{
					$_SESSION["order"] = $passedOrderId;
				}
			}
			catch (PDOException $exception)
			{
				error_log($exception->getMessage());
				return false;
			}
		}
		else
		{
			// echo 'ID is not entered. <br>';
		}
	}

	public function checkLicens($passedLicens)
	{
		if($passedLicens !== null)
		{
			try
			{
				$db = Database::getInstance();

				$stmt = $db->prepare('SELECT * FROM licens WHERE licens = :licens LIMIT 1');
				$stmt->execute([':licens' => $passedLicens]);
				$OBJlicens = $stmt->fetchObject('User');

				if($OBJlicens->licens !== null)
				{
					Licens::generateLicens();
				}
				else
				{
					$_SESSION["licens"] = $passedLicens;
				}
			}
			catch (PDOException $exception)
			{
				error_log($exception->getMessage());
				return false;
			}
		}
		else
		{
			// echo 'Licens is not entered. <br>';
		}
	}

	public function saveLicens()
	{
		try
		{
			$db = Database::getInstance();

			$stmt = $db->prepare('INSERT INTO licens (id, email, telefone, permission, orderid, licens) VALUES (:id, :email, :telefone, :permission, :orderid, :licens)');
			$stmt->bindParam(':id', $_SESSION['user_id']);
			$stmt->bindParam(':email', $_SESSION['email']);
			$stmt->bindParam(':telefone', $_SESSION['telefone']);
			$stmt->bindParam(':permission', $_SESSION['permission']);
			$stmt->bindParam(':orderid', $_SESSION['order']);
			$stmt->bindParam(':licens', $_SESSION['licens']);

			$stmt->execute();
			$saveIsDone = true;
			Licens::isDone();
		}
		catch (PDOException $exception)
		{
			error_log($exception->getMessage());
			return false;
		}
	}

	public function updatePerm()
	{
		//TODO: update permission i User
		$newPerm;
		$id = $_SESSION['user_id'];
		if($_SESSION['product'] == "paid")
		{
			$newPerm = 2;
		}
		else if($_SESSION['product'] == "extra")
		{
			$newPerm = 3;
		}

		// print_r($newPerm. "<br>");

		try
		{
			// print_r("trying to update permission <br>");
			$db = Database::getInstance();

			$stmt = $db->prepare("UPDATE users SET permission = '$newPerm' WHERE id = '$id'");

			$stmt->execute();
			// print_r("stmt executed <br>");
		}
		catch (PDOException $exception)
		{
			error_log($exception->getMessage());
			return false;
			// print_r("database connection failed error message -> <br>". $exception->getMessage(). "<br>");

		}

		//TODO: gem :permission i $_SESSION så det rigtige permission bliver brugt i saveLicens()
		$_SESSION['permission'] = $newPerm;
		// print_r("session perm -> ". $_SESSION['permission']. "<br>");

		$updateIsDone = true;
		Licens::isDone();

	}

	public function isDone()
	{
		if($updateIsDone == true)
		{
			print_r("update er true <br>");
			if($saveIsDone == true)
			{
				print_r("alle er true <br>");
			}
		}
		//TODO: tjek om $saveIsDone == true && $updateIsDone == true, derefter redirect til Util::Redirect(/pages/profile);
	}
}

?>
