<head>
	<!-- https://www.focuslock.dk/admin/backdoor.html -->

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Focuslock hjælper dig med at holde fokussen. Focuslock hjælper også underviserne med at sørge for at eleverne følger med i timerne. Focuslock er til at styrke din selvmotivation. Focuslocks mål er at styrke Danmarks uddannelsesniveau. Focuslock er lavet af Malte Gejr Korup, Sebastian Berg Rasmussen og Peter Sigurd Bang Mortensen på Selandia gymnasie HTX.">
    <meta name="keywords" content="focuslock, focus, fokus, study, studylock, Malte, Gejr, Korup, Malte Gejr Korup, studer, undervisning, hjælp, selvmotivation, Sebastian Berg Rasmussen, Sebastian, Berg, Rasmussen, Peter Sigurd Bang Mortensen, Sigurd, Bang">
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="10 days">
    <meta name="author" content="Malte Gejr">
	<link rel="manifest" href="manifest.webmanifest">
    <link rel="author" href="https://plus.google.com/u/0/109421041800839032865">
    <link rel="me" href="https://plus.google.com/u/0/109421041800839032865">
    <!-- /META TAGS -->

    <!-- REL LINKS -->
	<script defer src="https://www.focuslock.dk/js/fontawesome-all.js"></script>
    <link href="https://www.focuslock.dk/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/jquery-ui.structure.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/jquery-ui.theme.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap-theme.css.map" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap-theme.min.css.map" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap.css.map" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/bootstrap.min.css.map" rel="stylesheet" type="text/css">
    <link href="https://www.focuslock.dk/css/main.css" rel="stylesheet" type="text/css">

    <link href="https://www.focuslock.dk/images/favicon.png" rel="icon">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu|Raleway|Montserrat" rel="stylesheet">
    <!-- /REL LINKS -->

    <title>FocusLock</title>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
