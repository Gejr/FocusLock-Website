<?php
	session_start();
        require_once("includes/init.php");
?>
<html lang="da-DA">
	<?php include "includes/header.php" ?>

    	<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

        <?php include "includes/navbar.php" ?>

        <!-- Intro container start -->

	<header class="intro">
		<div class="intro-body">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">FocusLock</h1>
							<?php if (Auth::getInstance()->isLoggedIn()): ?>
		                        <h6 class="intro-text">
									<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-left"></i></span></sup>
									100% fokus til <?php echo htmlspecialchars($_SESSION["name"]); ?>
	                            	<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-right"></i></span></sup>
	                            </h6>
							<?php else: ?>
								<h6 class="intro-text">
									<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-left"></i></span></sup>
									100% fokus
									<sup><span class="fa-layers fa-fw"><i class="fas fa-quote-right"></i></span></sup>
								</h6>
							<?php endif; ?>

                        <div class="button-circle">
							<a href="#info" class="btn btn-circle page-scroll animated">
								<i class="fas fa-lock"></i>
							</a>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Intro container slut -->

	<!-- More Info Section Start -->
	<div class="info-section" id="info" >
		<section class="container content-section text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<h2>FocusLock</h2>
					<p>FocusLock er lavet til dem der har brug for lidt hjælp, til at fokusere på den igangværende opgave.</p>
					<p>Åben programmet, indtast den ønskede mængde tid, klik start og fokuser;<br>Så simpelt er det!</p>
					<p>Find os på de sociale medier.</p>
					<ul class="list-inline banner-social-buttons">
						<li>
							<a href="https://twitter.com/LockYourFocus" target="_blank" class="btn btn-default btn-lg">
							<i class="fab fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
						</li>
						<li>
							<a href="https://www.facebook.com/LockYourFocus" target="_blank" class="btn btn-default btn-lg">
							<i class="fab fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
						</li>
						<li>
							<a href="https://www.instagram.com/focuslock_app/" target="_blank" class="btn btn-default btn-lg">
							<i class="fab fa-instagram fa-fw"></i> <span class="network-name">Instagram</span></a>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div>

        <!-- More Info Section slut -->

	<?php include "includes/footer.php"?>
    </body>
</html>
