<?php

//Hash class

class Hash
{
    private static $_hasher;
    private function __construct() {}               //Tillader ikke at der laves en ny objekt af class'en med en ny hash
	private function __clone() {}                      //Tillader ikke at klone class'en

    //Laver hashet
    public static function make($text)
    {
        $salt = "e0071fa9";
        return $hashpassword =base64_encode(
        	hash('sha256', $salt . hash('sha256', $text, true), true)
            );
    }

    public static function check($text, $hash)
    {
        return password_verify($text, $hash);
    }
}

?>
