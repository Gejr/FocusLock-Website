<?php
//require("Database.class.php");
//require("Hash.class.php");

//User class

class User
{

	public $errors;

    public static function authenticate($email, $password)
    {
        $user = static::findByEmail($email);

        if($user !== null)
        {
            if(Hash::make($password) === $user->password)
            {
                return $user;
            }
            // else
		    // {
		    // 	echo 'Hash::make, error'."<br>";
		    // }
        }
        // else
        // {
        // 	echo 'authenticate failed, $user is null or incorrect'."<br>";
        // }
    }

    public static function authenticateC($email, $hashedpass)
    {
        $user = static::findByEmail($email);

        if($user !== null)
        {
            if($hashedpass === $user->password)
            {
                return $user;
            }
            else
		    {
		    	echo '$hashedpass is not $user->password'."<br>";
		    }
        }
        else
        {
        	echo 'authenticate failed, $user is null or incorrect'."<br>";
        }
    }

    public static function findByID($id)
    {
        try
        {
            $db = Database::getInstance();

            $stmt = $db->prepare('SELECT * FROM users WHERE id = :id LIMIT 1');
            $stmt->execute([':id'=>$id]);
            $user = $stmt->fetchObject('User');

            if($user !== false)
            {
                return $user;
            }
        }

    	catch(PDOException $exception)
        {
            error_log($exception->getMessage());
        }
    }

	public static function findMail($id)
    {
        try
        {
            $db = Database::getInstance();

            $stmt = $db->prepare('SELECT * FROM users WHERE id = :id LIMIT 1');
            $stmt->execute([':id'=>$id]);
            $user = $stmt->fetchObject('User');

            if($user !== false)
            {
                return $user->email;
            }
        }

    	catch(PDOException $exception)
        {
            error_log($exception->getMessage());
        }
    }

	public static function permGetter($email, $permission)
	{
		try
    	{
    		$db = Database::getInstance();

        	$stmt = $db->prepare("SELECT * FROM users WHERE email = :email");
    		$stmt->execute([':email' => $email]);
            $sub = $stmt->fetch(PDO::FETCH_ASSOC);
            $sub = $row['permission'];
		}

    	catch (PDOException $exception)
    	{
        	error_log($exception->getMessage());
        	return false;
    	}
	}

    public static function findByEmail($email)
    {
    	try
        {
            $db = Database::getInstance();

            $stmt = $db->prepare('SELECT * FROM users WHERE email = :email LIMIT 1');
            $stmt->execute([':email' => $email]);
            $user = $stmt->fetchObject('User');

            if ($user !== false)
            {
                return $user;
            }

            else
            {
            	echo '$username is not correct'."<br>";
            }
        }

        catch(PDOException $exception)
        {
            error_log($exception->getMessage());
        }
	}

    //Signup en ny bruger
    public static function signup($data)
    {
    	$user = new static();

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->telefone = $data['telefone'];
        $user->password = $data['password'];
        $user->passwordchck = $data['passwordchck'];
		$user->response = $_POST["g-recaptcha-response"];
		$user->unitid = $data['unitid'];

        if($user->isValid())
        {
            try
            {
                $db = Database::getInstance();

                $stmt = $db->prepare('INSERT INTO users (name, email, telefone, password, permission, unitid) VALUES (:name, :email, :telefone, :password, :permission, :unitid)');
                $stmt->bindParam(':name', $user->name);
                $stmt->bindParam(':email', filter_var($user->email, FILTER_SANITIZE_EMAIL));
                $stmt->bindParam(':telefone', $user->telefone);
				$stmt->bindParam(':password', Hash::make($user->password));
				$stmt->bindParam(':permission', User::permvalue($user->unitid));
				$stmt->bindParam(':unitid', $user->unitid);

                $stmt->execute();
				if(User::permvalue($user->unitid) == 1)
				{
					mkdir("../ftp/FocusLock/".User::permvalue($user->unitid)."/".$user->email, 0777, true);
				}
				else if(User::permvalue($user->unitid) == 2)
				{
					mkdir("../ftp/FocusLock/".User::permvalue($user->unitid)."/".$user->email, 0777, true);
				}
				else if (User::permvalue($user->unitid) == 3)
				{
					mkdir("../ftp/FocusLock/".User::permvalue($user->unitid)."/".$user->unitid."/".$user->email, 0777, true);
				}
            }

            catch(PDOException $exception)
            {
                //Logger exceptionen
                error_log($exception->getMessage());
				if(!mkdir("../ftp/FocusLock/".User::permvalue($user->unitid)."/".$user->email, 0777, true))
				{
					die("Failed to make folder");
				}
            }
        }
        return $user;
    }

    //Checker om der allerede findes en bruger med den spcifikke email
    public function emailExists($email)
    {
        try
        {
            $db = Database::getInstance();
            $stmt = $db->prepare('SELECT COUNT(*) FROM users WHERE email = :email LIMIT 1');
            $stmt->execute([':email' => $this->email]);

            $rowCount = $stmt->fetchColumn();
            return $rowCount == 1;
        }

        catch (PDOException $exception)
        {
            error_log($exception->getMessage());
            return false;
        }
    }

    //Checker om der allerede findes en bruger med den spcifikke telefon nummer
    public function telefoneExists($telefone)
    {
        try
        {
            $db = Database::getInstance();
            $stmt = $db->prepare('SELECT COUNT(*) FROM users WHERE telefone = :telefone LIMIT 1');
            $stmt->execute([':telefone' => $this->telefone]);

            $rowCount = $stmt->fetchColumn();
            return $rowCount == 1;
        }

        catch (PDOException $exception)
        {
            error_log($exception->getMessage());
        	return false;
        }
	}

	public function unitExists($unitid)
    {
        try
        {
            $db = Database::getInstance();
            $stmt = $db->prepare('SELECT COUNT(*) FROM units WHERE unitid = :unitid LIMIT 1');
            $stmt->execute([':unitid' => $this->unitid]);

            $rowCount = $stmt->fetchColumn();
            return $rowCount == 0;
        }

        catch (PDOException $exception)
        {
            error_log($exception->getMessage());
        	return false;
        }
	}

    //Checker om der er nogle fejl
    public function isValid()
    {
        $this->errors = [];

        //Navn
        if($this->name == '')
        {
            $this->errors['name'] = 'Indtast venligst et gyldigt navn.';
        }

        //Email
        if(filter_var($this->email, FILTER_VALIDATE_EMAIL) === false)
        {
            $this->errors['email'] = 'Indtast venligst en gyldig E-Mail addresse.';
        }
		if(strpos($this->email, '/') !== false)
		{
			$this->errors['email'] = 'Mailen må ikke indeholde ugyldige tegn.';
		}

        if($this->emailExists($this->email))
        {
            $this->errors['email'] = 'Denne E-Mail addresse er allerede i brug.';
        }

        //Telefon
    	if($this->telefone == '')
        {
            $this->errors['telefone'] = 'Indtast venligst et gyldigt telefon nummer.';
        }

        if($this->telefoneExists($this->telefone))
    	{
            $this->errors['telefone'] = 'Dette telefon nummer er allerede i brug.';
        }

        //Password
        if(strlen($this->password) < 5)
        {
            $this->errors['password'] = 'Indtast venligst et længere kodeord.';
        }

        if($this->password !== $this->passwordchck)
        {
    		$this->errors['password'] = 'De 2 kodeord skal være ens.';
        }

		//Recaptcha
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$data = array('secret' => '6LdT2isUAAAAAMGkpwBybE3FddiMAAxlbLRpLxGz', 'response' => $_POST["g-recaptcha-response"] );
		$options = array(
			'http' => array(
			'method' => 'POST',
			'content' => http_build_query($data)
			)
		);
		$context = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
		$captcha_success = json_decode($verify);

		if($captcha_success->success==false)
		{
			$this->errors['recaptcha'] = 'Du skal udfylde Anti-Spam rigtigt.';
		}

		//Unit ID
		if($this->unitExists($this->unitid))
        {
            $this->errors['unitid'] = 'Der findes ikke en afdeling med dette ID.';
        }
        return empty($this->errors);
    }
	
	//FIXME: Brug info fra downloadnow.php
	public function permvalue($unitid)
	{
		if($unitid == null || $unitid == 0)
		{
			return $perm = 1;
		}
		else if($unitid !== null || $unitid !== 0)
		{
			return $perm = 3;
		}
	}
}

?>
