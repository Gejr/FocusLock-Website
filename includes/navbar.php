<!-- Navbar start -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">

                <!-- Navbar-left start -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="https://www.focuslock.dk">
					<i class="fa fa-lock"></i> <b> FocusLock</b>
				</a>
			</div>
                <!-- Navbar-left slut -->

                <!-- Navbar-right start -->
			<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<li class="hidden">
						<a href="https://www.focuslock.dk"></a>
					</li>
					<li>
						<a class="page-scroll" href="https://www.focuslock.dk/pages/about"> <b>Om FocusLock  </b><i class="icon fas fa-info"></i></a>
					</li>
					<li>
						<a class="page-scroll" href="https://www.focuslock.dk/pages/solutions"> <b>Produkter  </b><i class="icon fas fa-window-maximize"></i></a>
					</li>
					<li>
						<a class="page-scroll" href="https://www.focuslock.dk/pages/contact"> <b>Kontakt os  </b ><i class="icon fas fa-phone"></i></a>
					</li>
                    <li>
						<a class="page-scroll" href="https://www.focuslock.dk/pages/support"> <b>Support </b><i class="iconx fas fa-medkit"></i></a>
					</li>
					<?php if (Auth::getInstance()->isLoggedIn()): ?>
						<li>
							<a class="page-scroll" href="https://www.focuslock.dk/pages/profile"> <b>Min profil  </b ><i class="icon fas fa-user "></i></a>
						</li>
					<?php else: ?>
						<li>
							<a class="page-scroll" href="https://www.focuslock.dk/pages/login"> <b>Log ind  </b ><i class="icon fas fa-sign-in "></i></a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
                <!-- Navbar-right slut -->

		</div>
	</nav>
<!-- Navbar slut -->
