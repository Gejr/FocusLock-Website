<?php
	session_start();
    require_once("../includes/init.php");

	if(isset($_POST['free']))
	{
		$_SESSION["product"] = "free";
		Util::redirect("/pages/checkout");
	}
	if(isset($_POST['paid']))
	{
		$_SESSION["product"] = "paid";
		Util::redirect("/pages/checkout");
	}
	if(isset($_POST['extra']))
	{
		$_SESSION["product"] = "extra";
		Util::redirect("/pages/checkout");
	}
?>

<?php if (Auth::getInstance()->isLoggedIn()): ?>

	<html lang="da-DA">
	    <?php include "../includes/header.php" ?>

	    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	    <?php include "../includes/navbar.php" ?>

	    <header class="profile">
			<div class="profile-body">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<h1 class="brand-heading">Din profil</h1>
							<h6 class="profile-text"></h6>

	                        <div class="button-circle">
								<a href="#scroll" class="btn btn-circle page-scroll animated">
								<i class="fas fa-lock"></i>
								</a>
	                        </div>
						</div>
					</div>
				</div>
			</div>
	    </header>

	    <div class="about-section" id="scroll">
	        <div class="what-section">
	            <section id="what-is" class="container content-section text-center">
	                <div class="about-focuslock">
	                    <div class="container">
	                        <div class="row">
	                            <div class="col-md-8 col-md-offset-2">
									<p>Hej <?php echo htmlspecialchars($_SESSION["name"]); ?>, velkommen til din profil.
										<br>
										Her kan du ændre din infomation og downloade FocusLock.
									</p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>



			<div class="info-section">
				<section id="info" class="container content-section text-center">
					<div class="info-focuslock">
						<div class="container">
	                        <div class="row">
	                            <div class="col-md-8 col-md-offset-2">
									<!-- TODO: ændre navn -->
									<!-- TODO: skift mail -->
									<!-- TODO: skift tlf nr. -->
									<!-- TODO: skift password -->

									<!-- Hvis forældre med udvidet funktion -->
									<!-- TODO: se og ændre gemte computer til ekstern kontrol -->

									<!-- IDEA: Linke mails sammen -->
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

			<div class="download-section">
				<section id="download" class="container content-section text-center">
					<div class="download-focuslock">
	                    <div class="container">
	                        <div class="row">
	                            <div class="col-md-8 col-md-offset-2">
									<p>Download FocusLock nu</p>
									<!-- TODO: Lav en checkout side -->

									<h3>Den personlige version.</h3>
									<p>Denne version er gratis,<br> den indenholder tidsfunktionen.</p>
									<ul class="list-inline banner-social-buttons">
										<li>
											<form method="post">
												<input type="submit" class="btn btn-default btn-lg" name="free" value="Download nu">
											</form>
										</li>
									</ul>

									<br><br>
									<h3>Forældre versionen.</h3>
									<p>Denne version koster 100 DKK,<br> den indenholder tidsfunktion og kalenderfunktionen.</p>
									<ul class="list-inline banner-social-buttons">
										<li>
											<form method="post">
												<input type="submit" class="btn btn-default btn-lg" name="paid" value="Køb nu">
											</form>
										</li>
									</ul>

									<br><br>
									<h3>Udvidet forældre version.</h3>
									<p>Denne version koster 150 DKK,<br> den indenholder normal pakken, og ekstern kontrol.</p>
									<ul class="list-inline banner-social-buttons">
										<li>
											<form method="post">
												<input type="submit" class="btn btn-default btn-lg" name="extra" value="Køb nu">
											</form>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>

		<?php include "../includes/footer.php" ?>

		</body>
	</html>
<?php else: ?>
	<script type="text/javascript">
		window.location.href = 'https://www.focuslock.dk';
	</script>
<?php endif; ?>
