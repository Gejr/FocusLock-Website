<?php

    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);


//Sign up en ny bruger

require_once("../includes/init.php");

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $user = User::signup($_POST);
    if(empty($user->errors))
    {
        //Redirect til signup success
        Util::redirect('/pages/signup_success');
    }
}
?>

<html lang="da-DA">

	<?php include "../includes/header.php" ?>


    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

		<?php include "../includes/signNavbar.php" ?>

		<header class="sign">
			<div class="sign-body">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<h1 class="sign-header">Opret</h1>
							<p class="sign-text">
								Du skal oprette en bruger for at kunne bruge FocusLock.
								<br>
								Du skal benytte brugeren til at logge ind på programmet.
							</p>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="form">
			<div class="form-body">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="errors">
								<?php if (isset($user)): ?>
									<ul>
										<?php foreach ($user->errors as $error): ?>
							        	<li><?php echo $error; ?></li>
							        	<?php endforeach; ?>
							    	</ul>
							    <?php endif; ?>
							</div>

							<form method="POST" class="formular">
							   	<div>
							       	<label for="name">Navn<span> *</span></label>
							       	<input id="name" name="name" value="<?php echo isset($user) ? htmlspecialchars($user->name) : ''; ?>"
										readonly type="name" onfocus="if (this.hasAttribute('readonly'))
										{
											this.removeAttribute('readonly');
											// fix for mobile safari to show virtual keyboard
											this.blur();
											this.focus();
										}"
									/>
							   	</div>

							    <div>
							      	<label for="email">Email<span> *</span></label>
							   		<input id="email" name="email" value="<?php echo isset($user) ? htmlspecialchars($user->email) : ''; ?>"
										readonly type="email" onfocus="if (this.hasAttribute('readonly'))
										{
											this.removeAttribute('readonly');
											// fix for mobile safari to show virtual keyboard
											this.blur();
											this.focus();
										}"
									/>
							    </div>

								<div>
							      	<label for="telefone">Tlf. nummer<span> *</span></label>
							    	<input id="telefone" name="telefone" value="<?php echo isset($user) ? htmlspecialchars($user->telefone) : ''; ?>"
										readonly type="telefone" onfocus="if (this.hasAttribute('readonly'))
										{
	    									this.removeAttribute('readonly');
	    									// fix for mobile safari to show virtual keyboard
	    									this.blur();
											this.focus();
										}"
									/>
							   	</div>

							   	<div>
							  		<label for="password">Kodeord<span> *</span></label>
							        <input type="password" id="password" name="password"
										readonly type="password" onfocus="if (this.hasAttribute('readonly'))
										{
											this.removeAttribute('readonly');
											// fix for mobile safari to show virtual keyboard
											this.blur();
											this.focus();
										}"
									/>
							  	</div>

							   	<div>
							       	<label for="passwordchck">Gentag Kodeord<span> *</span></label>
							       	<input type="password" id="passwordchck" name="passwordchck"
										readonly type="passwordchck" onfocus="if (this.hasAttribute('readonly'))
										{
											this.removeAttribute('readonly');
											// fix for mobile safari to show virtual keyboard
											this.blur();
											this.focus();
										}"
									/>
							   	</div>

								<div>
							      	<label for="unitid">Elev i en skole- eller ansat i en virksom som bruger FocusLock? Indtast deres kode her.</label>
							   		<input id="unitid" name="unitid" placeholder="Valgfrit" value="<?php echo isset($user) ? htmlspecialchars($user->unitid) : ''; ?>" />
							   	</div>

							   	<div>
							       	<label>(Anti-spam)<span> *</span></label>
							       	<div id="recaptcha" name="recaptcha" class="g-recaptcha" data-sitekey="6LdT2isUAAAAAJbLm2rW9NLKTiRLo1W5rIbh2CaA"></div>
							       	<br>
							      	<label id="msg" name="msg"></label>
							   	</div>

								<div class="termsofuse">
									<input class="checkbox" type="checkbox" name="termsofuse" value="termsofuse"> <label for="termsofuse"><a href="https://www.focuslock.dk/pages/termsofuse.html" target="_blank">Jeg acceptere hermed servicevilkårene.</a></label>
								</div>

							    <input id="submit" name="submit" type="submit" value="Opret bruger" />
							</form>

							<p class="info-text">
								Alt indtastet data er opbevaret og opfylder persondataloven.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	    <!-- <?php include "../includes/footer.php" ?> -->
    </body>
</html>
