<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

require_once("../includes/init.php");

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
        $email = $_POST['email'];
        if(Auth::getInstance()->login($email, $_POST['password']))
        {
                //Redirect til index
                Util::redirect('/pages/profile');
        }
}
?>

<html lang="da-DA">
	<?php include "../includes/header.php" ?>

	<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

		<?php include "../includes/logNavbar.php" ?>

		<header class="sign">
			<div class="sign-body">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<h1 class="sign-header">Log ind</h1>
							<p class="sign-text">

							</p>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="form">
			<div class="form-body">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="errors">
						        <?php if (isset($email)): ?>
					                <p>Forkert login</p>
						        <?php endif; ?>
							</div>

							<form method="POST" class="formular">
				                <div>
				                    <label for="email">E-Mail addresse</label>
				                   	<input id="email" name="email" value="<?php echo isset($email) ? htmlspecialchars($email) : ''; ?>" />
				                </div>

				                <div>
				                    <label for="password">Kodeord</label>
									<input type="password" id="password" name="password"/>
				                </div>
								<br><br>
				                <input type="submit" value="Log ind"/>
				        	</form>

							<p class="login-text">
								<a href="forgot_password">Glemt dit kodeord, klik her!</a><a class="invisible">............</a><a href="signup">Har du ikke en bruger, klik her!</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

    	<!-- <?php include "../includes/footer.php" ?> -->

    </body>
</html>
