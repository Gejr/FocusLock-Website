<?php

    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);

require_once("../includes/init.php");
?>

<html lang="da-DA">
	<?php include "../includes/header.php" ?>

    	<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

		<?php include "../includes/navbar.php" ?>
		<header class="success">
		<div class="success-body">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<h1 class="brand-heading">Registreret</h1>
						<h6 class="success-text">Brugeren er registreret, du vil blive sendt tilbage om 5 sekunder.</h6>

						<div class="button-circle">
							<a class="btn btn-circle page-scroll">
								<i class="fa fa-lock animated"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
        </header>

        <?php include "../includes/footer.php" ?>

		<!-- Tjekker om du bruger en windows maskine -->
        <script>
        	$(document).ready(function(){
				if(/Linux|UNIX|MacOS|Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
				{
					window.setTimeout(function(){ alert("Du skal bruge en Windows maskine for at downloade FocusLock.");}, 1000);
				}
				if(/Windows/i.test(navigator.userAgent))
				{
					window.setTimeout(function(){ location.href = "https://www.focuslock.dk/download/focuslock-installer.exe";}, 1000);
				}
	        	window.setTimeout(function(){
				location.href = "https://www.focuslock.dk/";
				}, 5000);
        	});
        </script>

        </body>
</html>
