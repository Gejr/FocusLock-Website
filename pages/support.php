<?php
	session_start();
    require_once("../includes/init.php")
?>
<?php
	$ticket = uniqid("#", false);
	$name = $_POST['name'];
	$email = $_POST['email'];
	$topic = $_POST['topic'];
	$message = $_POST['message'];
	$from = 'From: Support_FocusLock.dk';
	$to = 'support@focuslock.dk';

	$msg = $_POST['msg'];
	$robot = $_POST['g-recaptcha-response'];

	$fromback = 'From: Support@FocusLock.dk';
	$bodyback = "Tak for din mail. Vi har modtaget den og vi vender tilbage så hurtigt som muligt.\nGem venligst dit Ticket nummer: $ticket \n \nHilsen\nsupport@focuslock.dk\n(Du kan ikke svare tilbage på denne mail)";

	$body = "From: $name\n E-Mail: $email\n Topic: $topic\n Ticket number: $ticket\n Message:\n $message";
	$subject = "Topic: $topic - $ticket";

	if ($_POST['submit'])
	{
		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			//your site secret key
			$secret = '6Ld8riIUAAAAAP1SbTlk4JzZ382jE6t6pqDLnoLz';
			//get verify response data
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);

			if ($name != '' && $email != '' && $message != '')
			{
				if ($responseData->success)
				{
					if (mail ($to, $subject, $body, $from))
					{
						$msg = 'Tak, vi har nu modtaget din besked. Vi vender så hurtigt som muligt tilbage.';
						echo "<script type='text/javascript'>alert('$msg');</script>";
						mail($email, $subject, $bodyback, $fromback);
					} else {
						$msg = 'Et eller andet gik galt, prøv venligst igen.';
						echo "<script type='text/javascript'>alert('$msg');</script>";
					}
				} else if(!$responseData->success){
					$msg = 'Du har svaret forkert på Anti-Spam prøv venligst igen.';
					echo "<script type='text/javascript'>alert('$msg');</script>";
				}
			}else {
				$msg = 'Du skal udfylde alle de * makerede felter.';
				echo "<script type='text/javascript'>alert('$msg');</script>";
			}
		}else{
			$msg = 'Udflyd venligst Anti-Spam.';
			echo "<script type='text/javascript'>alert('$msg');</script>";
		}
	}
?>

<html lang="da-DA">
	<?php include "../includes/header.php" ?>

	<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include "../includes/navbar.php" ?>

	<header class="support">
		<div class="support-body">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<h1 class="brand-heading">Support</h1>
						<h6 class="support-text">Hjælpen når du har brug for den!</h6>

						<div class="button-circle">
							<a href="#support" class="btn btn-circle page-scroll animated">
								<i class="fas fa-lock"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="support-section" id="support">
	    	<div class="contact-section">
			<section id="contact-us" class="container content-section text-center">
		    	<div class="support-focuslock">
					<div class="container">
			    		<div class="row">
							<div class="col-md-8 col-md-offset-2">
				    			<h1 class="support-title">Brug for hjælp?</h1>
				    			<p class="support-text">Er der der noget der ikke dur, har du en ide til hvordan vi kan gøre
								programmet bedre eller har du fundet et spil vi ikke har blokeret endnu?
								<br> <br>
								Så udfyld denne formel så vender vi tilbage så hurtigt som muligt!
				    			</p>
							</div>
			    		</div>

						<form method="POST" action="support.php">
							<label>Navn <span>*</span></label>
							<input name="name" placeholder="Dit navn her">

							<label>E-Mail <span>*</span></label>
							<input name="email" type="email" placeholder="Din mail her">

							<label>Emne <span>*</span></label>
							<select id="topic" name="topic">
								<option value="" disabled selected>Vælg et emne</option>
								<option value="problem">Problem</option>
								<option value="bug">Fejl</option>
								<option value="idea">Ny funktion?</option>
								<option value="game">Manglende spil?</option>
							</select>

							<label>Besked <span>*</span></label>
							<textarea name="message" placeholder="Din besked"></textarea>

							<label>(Anti-spam) <span>*</span></label>
							<div id="recaptcha" name="recaptcha" class="g-recaptcha" data-sitekey="6Ld8riIUAAAAAPRgxdhees1jEjNFMd-duFPLny8C"></div>
							<br>
							<label id="msg" name="msg"></label>
							<input id="submit" name="submit" type="submit" value="Send">
						</form>
					</div>
		    	</div>
			</section>
		</div>
	</div>

	<?php include "../includes/footer.php" ?>

    </body>
</html
